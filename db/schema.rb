# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120404005752) do

  create_table "administrators", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username"
  end

  add_index "administrators", ["email"], :name => "index_administrators_on_email", :unique => true
  add_index "administrators", ["reset_password_token"], :name => "index_administrators_on_reset_password_token", :unique => true

  create_table "authentications", :force => true do |t|
    t.string   "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "galleryimages", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "item_file_name"
    t.string   "item_content_type"
    t.integer  "item_file_size"
    t.datetime "item_updated_at"
    t.string   "titleimage_file_name"
    t.string   "titleimage_content_type"
    t.integer  "titleimage_file_size"
    t.datetime "titleimage_updated_at"
    t.text     "copy"
    t.string   "fb_title"
    t.text     "fb_copy"
    t.text     "fb_link"
  end

  create_table "results", :force => true do |t|
    t.string   "name"
    t.text     "what_that_means"
    t.text     "friends_and_family"
    t.text     "in_nature"
    t.text     "beautiful_change_idea"
    t.string   "product_title"
    t.text     "product_description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "background_file_name"
    t.string   "background_content_type"
    t.integer  "background_file_size"
    t.datetime "background_updated_at"
    t.text     "facebook_subject"
    t.text     "facebook_copy"
    t.string   "product_file_name"
    t.string   "product_content_type"
    t.integer  "product_file_size"
    t.datetime "product_updated_at"
    t.string   "color"
    t.string   "header_file_name"
    t.string   "header_content_type"
    t.integer  "header_file_size"
    t.datetime "header_updated_at"
    t.integer  "product_image_xoffset"
    t.integer  "product_image_yoffset"
    t.string   "fb_share_image_link"
    t.string   "product_link"
    t.string   "whatthatmeansheading_file_name"
    t.string   "whatthatmeansheading_content_type"
    t.integer  "whatthatmeansheading_file_size"
    t.datetime "whatthatmeansheading_updated_at"
    t.string   "beautifulchangeideaheading_file_name"
    t.string   "beautifulchangeideaheading_content_type"
    t.integer  "beautifulchangeideaheading_file_size"
    t.datetime "beautifulchangeideaheading_updated_at"
    t.string   "friendsandfamilyheading_file_name"
    t.string   "friendsandfamilyheading_content_type"
    t.integer  "friendsandfamilyheading_file_size"
    t.datetime "friendsandfamilyheading_updated_at"
    t.string   "innatureheading_file_name"
    t.string   "innatureheading_content_type"
    t.integer  "innatureheading_file_size"
    t.datetime "innatureheading_updated_at"
  end

end
