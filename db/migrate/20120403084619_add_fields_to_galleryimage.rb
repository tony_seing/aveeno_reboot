class AddFieldsToGalleryimage < ActiveRecord::Migration
  def self.up
    add_column :galleryimages, :copy, :text
    add_column :galleryimages, :fb_title, :string
    add_column :galleryimages, :fb_copy, :text
    add_column :galleryimages, :fb_link, :text
    
    
    
  end

  def self.down
    remove_column :galleryimages, :copy
    remove_column :galleryimages, :fb_title
    remove_column :galleryimages, :fb_copy
    remove_column :galleryimages, :fb_link
    
  end
end
