class AddColorColumnToResults < ActiveRecord::Migration
  def change
    add_column :results, :color, :string
  end
end
