class AddAttachmentItemToGalleryimages < ActiveRecord::Migration
  def self.up
    add_column :galleryimages, :item_file_name, :string
    add_column :galleryimages, :item_content_type, :string
    add_column :galleryimages, :item_file_size, :integer
    add_column :galleryimages, :item_updated_at, :datetime
  end

  def self.down
    remove_column :galleryimages, :item_file_name
    remove_column :galleryimages, :item_content_type
    remove_column :galleryimages, :item_file_size
    remove_column :galleryimages, :item_updated_at
  end
end
