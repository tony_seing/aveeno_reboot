class AddColumnToResults < ActiveRecord::Migration
  def self.up
    add_column :results, :facebook_subject, :text
    add_column :results, :facebook_copy, :text
    
  end

  def self.down
    remove_column :results, :facebook_subject
    remove_column :results, :facebook_copy
  end
end
