module ActionController
	class Request
		alias_method :etag_matches_original?, :etag_matches?

		def etag_matches?(etag)
			!env['HTTP_USER_AGENT'].include?('MSIE') && etag_matches_original?(etag)

		end	
	end

	class Response
		alias_method :etag_original?, :etag?

		def etag?
			request.env['HTTP_USER_AGENT'].include?('MSIE') || etag_original?
		end
	end
end		