class AuthenticationsController < ApplicationController
  #before_filter :set_p3p

  # GET /authentications
  # GET /authentications.json
  def index
    @authentications = Authentication.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @authentications }
    end
  end

  # GET /authentications/1
  # GET /authentications/1.json
  def show
    @authentication = Authentication.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @authentication }
    end
  end

  # GET /authentications/new
  # GET /authentications/new.json
  def new
    @authentication = Authentication.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @authentication }
    end
  end

  # GET /authentications/1/edit
  def edit
    @authentication = Authentication.find(params[:id])
  end

  # POST /authentications
  # POST /authentications.json
  def create
       #@facebook_response = JSON.parse(request.env["omniauth.auth"].to_json) 
    @birthday = Date.strptime(request.env["omniauth.auth"]["extra"]["raw_info"]["birthday"], "%m/%d/%Y").month if request.env["omniauth.auth"]["extra"]["raw_info"]["birthday"].present?
    #  response = Net::HTTP.get_response("example.com","/?search=thing&format=json")
    
    
 
   
      @output = case
      when @birthday == 1
	"January result -- Calming Feverfew"
      when @birthday == 2
	"February result -- Protective Lotus"
      when @birthday == 3
	"March result -- Balancing Seaweed Extract"
      when @birthday == 4
	"April result -- Revitalizing Shiitake Mushroom"
      when @birthday == 5
	"May result -- Strengthening Southernwood"
      when @birthday == 6
	"June result -- Revitalizing Shiitake Mushroom"
      when @birthday == 7
	"July result -- Protective Lotus"
      when @birthday == 8
	"August result -- Vibrant Lupine"
      when @birthday == 9
	"September result -- Nurturing Oat"
      when @birthday == 10
	"October result -- Resilient Soy"
      when @birthday == 11
	"November result -- Balancing Seaweed Extract"
      when @birthday == 12
	"December result -- Focusing Wheat"
      else
	"Nurturing Oat -- Nurturing Oat"
      end
      
      
  #  render :text => request.env["omniauth.auth"].to_yaml
    
   render :text => @output
  
   # post to wall code below
   # redirect_to 'https://graph.facebook.com/' + request.env["omniauth.auth"]["uid"] + '/feed?access_token=' + request.env["omniauth.auth"]["credentials"]["token"] + '&message=apptest_tony'
    
   
  end

  # PUT /authentications/1
  # PUT /authentications/1.json
  def update
    @authentication = Authentication.find(params[:id])

    respond_to do |format|
      if @authentication.update_attributes(params[:authentication])
        format.html { redirect_to @authentication, notice: 'Authentication was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @authentication.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /authentications/1
  # DELETE /authentications/1.json
  def destroy
    @authentication = Authentication.find(params[:id])
    @authentication.destroy

    respond_to do |format|
      format.html { redirect_to authentications_url }
      format.json { head :ok }
    end
  end

   private
  
  # this is required by IE so that we can set session cookies
  def set_p3p
    headers['P3P'] = 'CP="ALL DSP COR CURa ADMa DEVa OUR IND COM NAV"'
  end
end
