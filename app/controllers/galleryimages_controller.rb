class GalleryimagesController < ApplicationController
    #before_filter :set_p3p

  # GET /galleryimages
  # GET /galleryimages.json
  def index
    @galleryimages = Galleryimage.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @galleryimages }
    end
  end

  # GET /galleryimages/1
  # GET /galleryimages/1.json
  def show
    @galleryimage = Galleryimage.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @galleryimage }
    end
  end

  # GET /galleryimages/new
  # GET /galleryimages/new.json
  def new
    @galleryimage = Galleryimage.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @galleryimage }
    end
  end

  # GET /galleryimages/1/edit
  def edit
    @galleryimage = Galleryimage.find(params[:id])
  end

  # POST /galleryimages
  # POST /galleryimages.json
  def create
    @galleryimage = Galleryimage.new(params[:galleryimage])

    respond_to do |format|
      if @galleryimage.save
        format.html { redirect_to @galleryimage, notice: 'Galleryimage was successfully created.' }
        format.json { render json: @galleryimage, status: :created, location: @galleryimage }
      else
        format.html { render action: "new" }
        format.json { render json: @galleryimage.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /galleryimages/1
  # PUT /galleryimages/1.json
  def update
    @galleryimage = Galleryimage.find(params[:id])

    respond_to do |format|
      if @galleryimage.update_attributes(params[:galleryimage])
        format.html { redirect_to @galleryimage, notice: 'Galleryimage was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @galleryimage.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /galleryimages/1
  # DELETE /galleryimages/1.json
  def destroy
    @galleryimage = Galleryimage.find(params[:id])
    @galleryimage.destroy

    respond_to do |format|
      format.html { redirect_to galleryimages_url }
      format.json { head :ok }
    end
  end

  private

  # this is required by IE so that we can set session cookies
  def set_p3p
    headers['P3P'] = 'CP="ALL DSP COR CURa ADMa DEVa OUR IND COM NAV"'
  end
end
