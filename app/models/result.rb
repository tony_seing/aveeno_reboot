class Result < ActiveRecord::Base
    has_attached_file :background
    has_attached_file :product
    has_attached_file :header
    has_attached_file :whatthatmeansheading
    has_attached_file :friendsandfamilyheading
    has_attached_file :innatureheading
    has_attached_file :beautifulchangeideaheading
   
end
